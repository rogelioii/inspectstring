FROM ubuntu:17.10

MAINTAINER rogelioii@gmail.com

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
    httpie \
	git \
	python3 \
	python3-dev \
	python3-setuptools \
	python3-pip \
	nginx \
	supervisor \
	vim \
	sqlite3 && \
	pip3 install -U pip setuptools && \
    rm -rf /var/lib/apt/lists/*

# Set Timezone to Central
ENV TZ=America/Chicago
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

#Install gunicorn
RUN pip3 install gunicorn

# Setup nginx
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
RUN rm /etc/nginx/sites-enabled/default
COPY nginx.conf /etc/nginx/sites-available/default
RUN ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default

# Setup flask application
RUN mkdir -p /deploy/app
COPY app /deploy/app
RUN pip3 install -r /deploy/app/requirements.txt

RUN ln -sf /var/log/gunicorn.access.log  /dev/stdout
RUN ln -sf /var/log/gunicorn.error.log  /dev/stdout

# Setup supervisord
RUN mkdir -p /var/log/supervisor
COPY supervisord.conf /etc/supervisor/conf.d/

CMD ["/usr/bin/supervisord"]
