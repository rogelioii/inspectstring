## Instructions
You will receive a string as input, potentially a mixture of upper and lower case, numbers, special characters etc. The task is to determine if the string contains at least one of each letter of the alphabet. Return true if all are found and false if not. Write it as a RESTful web service (no authentication necessary) in any language/framework you choose and document the service. 

## Setup

### Create Virtual Environment:
* virtualenv --python=/usr/local/bin/python3.6 env

### Activate Virtual Environment
* source env/bin/activate

### Install Requirements on your local machine
* pip install -r app/requirements.txt

## Run in development environment:
* python ./app/inspectstring_api.py

* or use gunicorn (from the app directory)

* gunicorn inspectstring_api:app -b localhost:10000

## Build Using Docker
* docker build -t inspect_api .

### Run Docker Locally
* docker run --name inspect_api -p 10000:6000 inspect_api:latest


### See API Documentation and interactive:
* http://HOSTNAME:10000/apidocs


# Design Considerations
* Given that this is a simple API project without the need for authenication and backend database, I opted for the simplest Flask example using Flasgger.  As such, I did not break up the directory structure into a more traditional Flask app structure.

* If I were to add APIs to this project, then I would use either of the following methods:
    * Method 1: Use Traditional Flask Directory structure that includes the following directories:
    * migrations - Contains database migrations if we're connected to a backend database (i.e. Postgres)
    * models - ORM definitions of entities in the database.
    * repositories - Serves as 'DAO' interface of the models, where you can execute CRUD.
    * routes - A more modular and cleaner organization for api routing.
    * swagger - Can contain the API documentation in this directory, decorators used on the actual functions will reference this data.
    * refer: https://github.com/antkahn/flask-api-starter-kit
    
* Method 2: Use swaggerhub to define the APIs, then generate the python flask shell where all that's left to do is implement the business logic
    * routes/controllers are defined within swagger.yml files


#### Misc: 
* Remove all containers: 
* docker rm $(docker ps -aq)
