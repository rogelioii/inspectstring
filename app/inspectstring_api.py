import json
from flask import Flask, request, jsonify
from flasgger import Swagger
from flasgger.utils import swag_from
from flasgger import LazyString, LazyJSONEncoder

app = Flask(__name__)
app.config["SWAGGER"] = {"title": "Inspect String Documentation UI", "uiversion": 2}
app.json_encoder = LazyJSONEncoder
swagger = Swagger(app)

def is_pangram(input):
    # covert input string into lower case 
    input = input.lower() 
      
    # convert input string into Set() so that we will 
    # list of all unique characters present in sentence 
    input = set(input) 
  
    # separate out all alphabets 
    # ord(ch) returns ascii value of of character 
    alpha = [ ch for ch in input if ord(ch) in range(ord('a'), ord('z')+1) ] 
  
    return len(alpha) == 26

@app.route("/")
def index():
    """Hello World Example.

    ---
    responses:
      200:
        description: OK
    """

    return "String Inspector!"

@app.route("/inspect", methods=["POST"])
# @swag_from("swagger_config.yml") -- Instead of splitting the doc into another file, I've chosen to keep it inline.
def inspect():
    """Inspect a String.

    Given a string that can potentially be a mixture of upper and lower case, numbers, special characters etc. This API will return a dictionary with two parameters: `success` which indicates a successful API call, and a `contains_all_alphabet` which indicates the string inspector found each letter of the alphabet within the input string.
    ---
    consumes:
      - "application/json"
    produces:
      - "application/json"
    parameters:
    - in: "body"
      name: "body"
      description: "Accepts JSON with `input_string` parameter"
      required: true
      schema:
        type: object
        properties:
          input_string:
            type: string
            example : "The quick brown fox now jumps over the lazy dog."
    responses:
      200:
        description: OK
        name: Valid Response
        schema:
          type: object
          properties:
            success:
              type: boolean
            contains_all_alphabet:
              type: boolean
      405:
        description: "Invalid input"
    """
    input_json = request.get_json()
    print('input_json: {}'.format(input_json))
    if not input_json:
      raise Exception('Requires Body')
    try:
      input_string = input_json["input_string"]
      res = is_pangram(input_string)
      print('res:{} ==> {}'.format(input_string, res))
    except Exception as e:
      print(e)
      res = {"success": False, "message": str(e)}

    return json.dumps({"success": True, "contains_all_alphabet": res})


if __name__ == "__main__":
    app.run(debug=True, port=5000)
