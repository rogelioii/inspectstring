import pytest
from inspectstring_api import is_pangram

def test_is_pangram_letters_only_true():
    assert is_pangram('the quick brown fox jumps over the lazy dog')

def test_is_pangram_letters_only_false():
    assert False == is_pangram('the quick brown fox jumps over the lay dog')

def test_is_pangram_with_special_characters_true():
    assert is_pangram('~!@#$%^^&*() the quick brown fox jumps over the lazy dog')

def test_is_pangram_with_numbers_true():
    assert is_pangram('88 the 77 quick brown 66 fox jumps over the lazy dog')

def test_is_pangram_with_special_and_numbers_true():
    assert is_pangram('the quick brown fo!@#$!@%^7784x jumps over the lazy dog')

def test_is_pangram_with_special_characters_false():
    assert False == is_pangram('the qu^%$##(((&ick brown fo jumps over the lazy dog')

def test_is_pangram_with_numbers_false():
    assert False == is_pangram('the quick brown f79843759734ox jumps over the lay dog')

def test_is_pangram_with_special_and_numbers_false():
    assert False == is_pangram('the quick bro!@#%^$%^$%%$8765432n fox jumps over the lay dog')
