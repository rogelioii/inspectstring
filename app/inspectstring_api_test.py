import os
import json
import tempfile
import pytest
import inspectstring_api as api

@pytest.fixture
def client():
    client = api.app.test_client()
    yield client

def test_no_body(client):
    rv = client.post('/inspect')
    assert rv.status_code == 500

def test_with_good_body_letters_only(client):
    input_string = 'the quick brown fox now jumps over the lazy dog'
    rv = client.post('/inspect', json={'input_string': input_string})
    assert rv.status_code == 200
    assert json.loads(rv.data)['contains_all_alphabet']

def test_with_good_body_capital_letters_only(client):
    input_string = 'the quick brown fox now jumps over the lazy dog'.upper()
    rv = client.post('/inspect', json={'input_string': input_string})
    assert rv.status_code == 200
    assert json.loads(rv.data)['contains_all_alphabet']

def test_with_bad_body(client):
    rv = client.post('/inspect', data='this is bad data')
    assert rv.status_code == 500

def test_with_good_body_capital_letters_only_false(client):
    input_string = 'the quick brown fox now jumps over the lay dog'.upper()
    rv = client.post('/inspect', json={'input_string': input_string})
    assert rv.status_code == 200
    assert False == json.loads(rv.data)['contains_all_alphabet']